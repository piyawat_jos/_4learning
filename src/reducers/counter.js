var initialState = { couter: 0, text: "" };

function counterReducer(state = initialState, action) {
  switch (action.type) {
    case "INCREMENT":
      return {
        couter: state.couter + 1,
        text: action.text
      };
    case "DECREMENT":
      return {
        couter: state.couter - 1,
        text: action.text
      };
    default:
      return state;
  }
}

export default counterReducer;
